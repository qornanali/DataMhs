#include "header.h"

P_Mhs allocMhs(){
	P_Mhs P = (P_Mhs) malloc(sizeof(O_Mhs));
	Nim(P) = newString(10);
	Nama(P) = newString(60);
	TempatLahir(P) = newString(20);
	TglLahir(P) = newString(20);
	Alamat(P) = newString(60);
	Telp(P) = newString(15);
	Prodi(P) = newString(20);
	Kelas(P) = newString(5);
	Next(P)  = NULL;
	return P;
}

void DeAllocMhs (P_Mhs P){
	if (P != NULL){
		free (P);
	}
}

int getlastIdMhs(){
	
	int id_Mhs;
	String S = newString(1000);
	S[0]=NULL;
	char tmp;
	int count=0;
	
	FILE *fptr;
	if ((fptr=fopen("t_Mhs.txt","r"))==NULL){
		 printf("Tidak bisa membuka t_Mhs.txt!");
		 return NULL;
	}else{
		int j=0;
		do {
			tmp = fgetc(fptr);
			if(tmp==';'){
				if(j==0){
					id_Mhs = atoi(S);
				}
				j++;
				S[0]=NULL;		   
			}
			else if (tmp=='|'){
				count++;
				j=0;
				S[0]=NULL;
			}else{
				snprintf(S,1000,"%s%c",S,tmp);
			}
		}while(!feof(fptr));
		fclose(fptr);
		return id_Mhs;
	}
}

void addMhs(){
	s("cls");
	
	O_Mhs Mhs;
	Mhs.nim = newString(10);
	Mhs.nama = newString(60);
	Mhs.tempat_lahir = newString(20);
	Mhs.tgl_lahir = newString(20);
	Mhs.alamat = newString(60);
	Mhs.telp = newString(15);
	Mhs.prodi = newString(20);
	Mhs.kelas = newString(5);
	printf("Nim : ");
	fflush(stdin);
	fgets(Mhs.nim,10,stdin);
	printf("Nama : ");
	fflush(stdin);
	fgets(Mhs.nama,60,stdin);
	printf("JK : ");
	fflush(stdin);
	scanf("%c",&Mhs.jk);
	printf("Tempat Lahir : ");
	fflush(stdin);
	fgets(Mhs.tempat_lahir,20,stdin);
	printf("Tanggal Lahir : ");
	fflush(stdin);
	fgets(Mhs.tgl_lahir,20,stdin);
	printf("Alamat : ");
	fflush(stdin);
	fgets(Mhs.alamat,60,stdin);
	printf("No. Telp : ");
	fflush(stdin);
	fgets(Mhs.telp,15,stdin);
	printf("Prodi : ");
	fflush(stdin);
	fgets(Mhs.prodi,20,stdin);
	printf("Kelas : ");
	fflush(stdin);
	fgets(Mhs.kelas,5,stdin);
	printf("Angkatan : ");
	fflush(stdin);
	scanf("%d",&Mhs.angkatan);
	Mhs.id_mhs = getlastIdMhs()+1;
//	printf(
//	 "%s;%s;%c;%s;%s;%s;%s;%s;%s;%d;|\n",
//			Mhs.id_mhs,Mhs.nim,Mhs.jk,Mhs.tempat_lahir,Mhs.tgl_lahir,Mhs.alamat,Mhs.telp,Mhs.prodi,Mhs.kelas,Mhs.angkatan);
	insertMhs(Mhs);
}

void selectMhs(){
	s("cls");
	L_Mhs list_Mhs;
	printf("Jumlah Data = %d\n\n",syncListMhs(&list_Mhs));
	
	P_Mhs P = First(list_Mhs);
	while(P != NULL){
		printf("Id Mahasiswa : %d\nNim : %s\nNama : %s\nJK : %c\nTempat Lahir : %s\nTanggal Lahir : %s\nAlamat : %s\nNo. Telp : %s\nProdi : %s\nKelas : %s\nAngkatan : %d\n\n",
			IdMhs(P),Nim(P),Nama(P),JK(P),TempatLahir(P),TglLahir(P),Alamat(P),Telp(P),Prodi(P),Kelas(P),Angkatan(P));
		P = Next(P);
	}
}

void printListMhstoFile(L_Mhs L){
	P_Mhs P = First(L);	
	String data = newString(1000);
//	data[0] = NULL;
	FILE *fptr;
	if ((fptr=fopen("t_Mhs.txt","r"))==NULL){
 		printf("Tidak bisa membuka t_Mhs.txt!");
	}else{
		fclose(fptr);
		fptr=fopen("t_Mhs.txt","w");
		while(P != NULL){
			fprintf(fptr, "%d;%s;%s;%c;%s;%s;%s;%s;%s;%s;%d;|\n",
			IdMhs(P),Nim(P),Nama(P),JK(P),TempatLahir(P),TglLahir(P),Alamat(P),Telp(P),Prodi(P),Kelas(P),Angkatan(P));
			P = Next(P);
		}
		fclose(fptr);
	}
}

P_Mhs searchMhs(L_Mhs L, int id_Mhs){
	P_Mhs P = First(L);
 	int found = 0;
 	while ((P != NULL) && (found == 0)){
 		if(IdMhs(P) == id_Mhs){
 			found = 1;
		}else{
			P = Next(P);
		}
 	}
 	return (P);
}

void removeMhs(){
	s("cls");
	int id_Mhs;
	printf("Id Mhs : ");
	scanf("%d",&id_Mhs);
	L_Mhs list;
	syncListMhs(&list);
	deleteMhs(&list,id_Mhs);
	printListMhstoFile(list);
}

void updateMhs(){
	s("cls");
	int id_Mhs;
	printf("Id Mhs : ");
	scanf("%d",&id_Mhs);
	L_Mhs L;
	syncListMhs(&L);
	P_Mhs P = searchMhs(L,id_Mhs);
	if(P == NULL){
		printf("Tidak ditemukan Data dengan Id Mhs : %d",id_Mhs);
	}else{
		printf("Nim : ");
		fflush(stdin);
		fgets(Nim(P),10,stdin);
		printf("Nama : ");
		fflush(stdin);
		fgets(Nama(P),60,stdin);
		printf("JK : ");
		fflush(stdin);
		scanf("%c",&JK(P));
		printf("Tempat Lahir : ");
		fflush(stdin);
		fgets(TempatLahir(P),20,stdin);
		printf("Tanggal Lahir : ");
		fflush(stdin);
		fgets(TglLahir(P),20,stdin);
		printf("Alamat : ");
		fflush(stdin);
		fgets(Alamat(P),60,stdin);
		printf("No. Telp : ");
		fflush(stdin);
		fgets(Telp(P),15,stdin);
		printf("Prodi : ");
		fflush(stdin);
		fgets(Prodi(P),20,stdin);
		printf("Kelas : ");
		fflush(stdin);
		fgets(Kelas(P),5,stdin);
		printf("Angkatan : ");
		scanf("%d",&Angkatan(P));
		printListMhstoFile(L);
	}
}

void deleteMhs(L_Mhs * L, int id_Mhs){
	P_Mhs PDel = First(*L);
	P_Mhs Prec = NULL;
	int found = 0;
	while(PDel != NULL && found == 0){
		if(IdMhs(PDel) == id_Mhs){
			found = 1;
		}else{
			Prec = PDel;
			PDel = Next(PDel);	
		}
	}
	if(found == 1){
		if (Prec == NULL && Next(PDel) == NULL){ 
			First(*L) = NULL; 
		}else 
		if (Prec == NULL){ 
			First(*L) = Next(PDel); 
		}else{ 
			Next(Prec) = Next(PDel); 
		}
 		Next(PDel) = NULL;
 		DeAllocMhs(PDel);
	}else{
		printf("Tidak ditemukan Data dengan Id Mhs : %d",id_Mhs);
	}	
}

void insertMhs(O_Mhs X){
 	String data = newString(1000);
 	snprintf(data, sizeof(char)*1000, 
	 "%d;%s;%s;%c;%s;%s;%s;%s;%s;%s;%d;|\n",
			X.id_mhs,X.nim,X.nama,X.jk,X.tempat_lahir,X.tgl_lahir,X.alamat,X.telp,X.prodi,X.kelas,X.angkatan);
	FILE *fptr;
 	fptr=fopen("t_Mhs.txt", "a");
 	if(fptr==NULL){
 		printf("Tidak bisa membuka t_Mhs.txt!");
 	}else{
		fprintf(fptr, "%s", data);
		fclose(fptr);
	}
}

void createListMhs(L_Mhs * L){
	First((*L)) = NULL;
}

int syncListMhs(L_Mhs * L){
	
	createListMhs(L);
	
	P_Mhs P,PData;
	PData = First((*L));
	String S = newString(1000);
	S[0]=NULL;
	char tmp;
	int count=0;
	
	FILE *fptr;
	if ((fptr=fopen("t_Mhs.txt","r"))==NULL){
		 printf("Tidak bisa membuka t_Mhs.txt!");
	}else{
		P = allocMhs();
		int j=0;
		do {
			tmp = fgetc(fptr);
			if(tmp==';'){
				switch(j){
					case 0 :
						IdMhs(P) = atoi(S);
						break;
					case 1 :
						strcpy(Nim(P),S);
						break;
					case 2 :
						strcpy(Nama(P),S);
						break;
					case 3 :
						JK(P) = S[0];
						break;
					case 4 :
						strcpy(TempatLahir(P),S);
						break;
					case 5 :
						strcpy(TglLahir(P),S);
						break;
					case 6 :
						strcpy(Alamat(P),S);
						break;
					case 7 :
						strcpy(Telp(P),S);
						break;
					case 8 :
						strcpy(Prodi(P),S);
						break;
					case 9 :
						strcpy(Kelas(P),S);
						break;
					case 10 :
						Angkatan(P) = atoi(S);
						break;
				}
			j++;
			S[0]=NULL;		   
			}
			else if (tmp=='|'){
				count++;
				j=0;
				if(PData==NULL){
					First((*L)) = P;
					PData = P;
				}
				else{
					Next(PData) = P;
					PData = Next(PData);
				}
				P = allocMhs();
				S[0]=NULL;
			}else{
				snprintf(S,1000,"%s%c",S,tmp);
			}
		}while(!feof(fptr));
		P_Mhs tmp1 = First((*L));
		fclose(fptr);
	}
	return count;
}
