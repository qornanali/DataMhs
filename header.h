#include <stdio.h>
#include <limits.h>
#include <malloc.h>
#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

#define Next(P) (P)->next
#define First(L) (L).first

#define IdMhs(P) (P)->id_mhs
#define Nim(P) (P)->nim
#define Nama(P) (P)->nama
#define JK(P) (P)->jk
#define TempatLahir(P) (P)->tempat_lahir
#define TglLahir(P) (P)->tgl_lahir
#define Alamat(P) (P)->alamat
#define Telp(P) (P)->telp
#define Prodi(P) (P)->prodi
#define Kelas(P) (P)->kelas
#define Angkatan(P) (P)->angkatan

#ifndef header_H

//string
typedef char * String;

//t_order
typedef struct O_Mhs * P_Mhs;
typedef struct O_Mhs{
 	int id_mhs;
 	String nim;
 	String nama;
 	char jk;
 	String tempat_lahir;
 	String tgl_lahir;
 	String alamat;
 	String telp;
 	String prodi;
 	String kelas;
 	int angkatan;
	P_Mhs next;
} O_Mhs;
typedef struct{
	P_Mhs first;
} L_Mhs;

//list_order.c
P_Mhs allocMhs();
void DeAllocMhs (P_Mhs P);
P_Mhs searchMhs(L_Mhs L, int id_Mhs);
void insertMhs(O_Mhs X);
void updateMhs();
void deleteMhs(L_Mhs * L, int id_Mhs);
void insertMhs(O_Mhs X);
void selectMhs();
void addMhs();
void removeMhs();
void createListMhs(L_Mhs * L);
int syncListMhs(L_Mhs * L);
void printListMhstoFile(L_Mhs L);
int getlastIdMhs();

//file.c
char * newString(int size);
void rewritefile(int id_file);
String readFile(int id_file);
void s(String c);

#endif
